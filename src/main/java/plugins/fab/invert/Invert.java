package plugins.fab.invert;

import java.util.ArrayList;

import icy.common.listener.ProgressListener;
import icy.image.IcyBufferedImage;
import icy.sequence.Sequence;
import icy.type.DataType;
import icy.type.collection.array.Array1DUtil;
import icy.util.OMEUtil;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzGUI;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzStoppable;
import plugins.adufour.ezplug.EzVarSequence;

/**
 * Inverts data in the image. ( values are inverted between min and max ie: min and max will still be the same at the end of the process )
 * 
 * @author Fabrice de Chaumont
 * @author Stephane Dallongeville
 */
public class Invert extends EzPlug implements Block, EzStoppable
{
    private EzVarSequence input = new EzVarSequence("Sequence");
    private EzVarSequence output = new EzVarSequence("Result");

    @Override
    protected void initialize()
    {
        addEzComponent(input);
        output.setValue(null);
    }

    @Override
    public void clean()
    {
        //
    }

    @Override
    public void execute()
    {
        final ProgressListener progressListener;
        final EzGUI ui = getUI();

        // display progression if not in headless
        if (ui != null)
        {
            progressListener = new ProgressListener()
            {
                @Override
                public boolean notifyProgress(double position, double length)
                {
                    ui.setProgressBarValue(position / length);
                    return true;
                }
            };
        }
        else
            progressListener = null;

        final Sequence sequence = input.getValue();
        Sequence result = null;

        if (sequence != null)
            result = createInvert(sequence, progressListener);

        if (ui != null)
            ui.setProgressBarValue(0);

        output.setValue(result);

        if (!isHeadLess())
            addSequence(result);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("Sequence", input.getVariable());
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("Result", output.getVariable());
    }

    public static Sequence createInvert(Sequence sequence, ProgressListener listener)
    {
        if (sequence == null)
            return null;

        final int sizeX = sequence.getSizeX();
        final int sizeY = sequence.getSizeY();
        final int sizeC = sequence.getSizeC();
        final int sizeZ = sequence.getSizeZ();
        final int sizeT = sequence.getSizeT();
        final DataType dataType = sequence.getDataType_();

        final double[] minAndMax = sequence.getChannelsGlobalTypeBounds();
        final double min = minAndMax[0];
        final double max = minAndMax[1];

        final Sequence result = new Sequence(OMEUtil.createOMEXMLMetadata(sequence.getOMEXMLMetadata()));

        final int size = sizeT * sizeZ * sizeC;
        int ind = 0;

        result.beginUpdate();
        try
        {
            for (int t = 0; t < sizeT; t++)
            {
                for (int z = 0; z < sizeZ; z++)
                {
                    final IcyBufferedImage imageSrc = sequence.getImage(t, z);
                    final IcyBufferedImage imageDst = new IcyBufferedImage(sizeX, sizeY, sizeC, dataType);

                    for (int c = 0; c < sizeC; c++)
                    {
                        // interrupted ? --> return incomplete result..
                        if (Thread.interrupted())
                            return result;

                        if (listener != null)
                            listener.notifyProgress(ind, size);

                        // convert to double for easier manipulation
                        final double[] data = Array1DUtil.arrayToDoubleArray(imageSrc.getDataXY(c),
                                dataType.isSigned());

                        // invert
                        for (int i = 0; i < data.length; i++)
                            data[i] = min + (max - data[i]);

                        final Object dest = imageDst.getDataXY(c);
                        // convert back to original datatype
                        Array1DUtil.doubleArrayToArray(data, dest);
                        // ensure to properly save data in cache (virtual mode)
                        imageDst.setDataXY(c, dest);

                        result.setImage(t, z, imageDst);
                        ind++;
                    }
                }
            }
        }
        finally
        {
            result.endUpdate();
        }

        // preserve channel informations
        for (int c = 0; c < sizeC; c++)
        {
            result.setChannelName(c, sequence.getChannelName(c));
            result.setDefaultColormap(c, sequence.getDefaultColorMap(c), true);
            // it's important to set user colormap after 'endUpdate' as it will internally create a new 'user LUT'
            // based on current channel bounds
            result.setColormap(c, sequence.getColorMap(c));
        }

        return result;
    }

    public static void invert(Sequence sequence)
    {
        if (sequence == null)
            return;

        double[] minAndMax = sequence.getChannelsGlobalTypeBounds();

        double min = minAndMax[0];
        double max = minAndMax[1];

        ArrayList<IcyBufferedImage> images = sequence.getAllImage();

        for (IcyBufferedImage image : images)
        {
            for (int channel = 0; channel < images.get(0).getSizeC(); channel++)
            {

                double[] resultDataBuffer = Array1DUtil.arrayToDoubleArray(image.getDataXY(channel),
                        image.isSignedDataType());
                for (int i = 0; i < resultDataBuffer.length; i++)
                {
                    resultDataBuffer[i] = min + max - resultDataBuffer[i];
                }
                Array1DUtil.doubleArrayToArray(resultDataBuffer, image.getDataXY(channel));
            }
        }

        sequence.dataChanged();
    }
}
